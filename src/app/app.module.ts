import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IndexPageComponent } from './index-page/index-page.component';
import { ImpressumPageComponent } from './impressum-page/impressum-page.component';
import { DatenschutzPageComponent } from './datenschutz-page/datenschutz-page.component';
import { KontaktPageComponent } from './kontakt-page/kontakt-page.component';
import { AboutMePageComponent } from './about-me-page/about-me-page.component';
import { MediengestaltungPageComponent } from './mediengestaltung-page/mediengestaltung-page.component';
import { FotografiePageComponent } from './fotografie-page/fotografie-page.component';
import { MedienkompetenzPageComponent } from './medienkompetenz-page/medienkompetenz-page.component';
import { PDFComponent } from './pdf/pdf.component';
import {APP_BASE_HREF} from "@angular/common";

@NgModule({
  declarations: [
    AppComponent,
    IndexPageComponent,
    ImpressumPageComponent,
    DatenschutzPageComponent,
    KontaktPageComponent,
    AboutMePageComponent,
    MediengestaltungPageComponent,
    FotografiePageComponent,
    MedienkompetenzPageComponent,
    PDFComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
