import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {IndexPageComponent} from "./index-page/index-page.component";
import {ImpressumPageComponent} from "./impressum-page/impressum-page.component";
import {DatenschutzPageComponent} from "./datenschutz-page/datenschutz-page.component";
import {KontaktPageComponent} from "./kontakt-page/kontakt-page.component";
import {AboutMePageComponent} from "./about-me-page/about-me-page.component";
import {MediengestaltungPageComponent} from "./mediengestaltung-page/mediengestaltung-page.component";
import {FotografiePageComponent} from "./fotografie-page/fotografie-page.component";
import {MedienkompetenzPageComponent} from "./medienkompetenz-page/medienkompetenz-page.component";

const routes: Routes = [
    {
        path: 'impressum',
        component: ImpressumPageComponent,
    },
    {
        path: 'datenschutz',
        component: DatenschutzPageComponent,
    },
    {
        path: 'kontakt',
        component: KontaktPageComponent,
    },
    {
        path: 'aboutme',
        component: AboutMePageComponent,
    },
    {
        path: 'mediengestaltung',
        component: MediengestaltungPageComponent,
    },
    {
        path: 'fotografie',
        component: FotografiePageComponent,
    },
    {
        path: 'medienkompetenz',
        component: MedienkompetenzPageComponent,
    },
    {
        path: '**',
        component: IndexPageComponent,
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
