import {AfterViewInit, Component} from '@angular/core';
import {TabsService} from "../tabs.service";

@Component({
    selector: 'app-mediengestaltung-page',
    templateUrl: './mediengestaltung-page.component.html',
    styleUrls: ['./mediengestaltung-page.component.scss']
})
export class MediengestaltungPageComponent implements AfterViewInit {

    constructor(private tabs: TabsService) {
        tabs.setTabs([
            {
                id: 'tab1',
                title: 'Bewegungsablauf',
            },
            {
                id: 'tab2',
                title: 'Farbwirkung mit Begriffen',
            },
            {
                id: 'tab3',
                title: 'Typografie, Schriften',
            },
            {
                id: 'tab4',
                title: 'Raster',
            },
        ]);
    }

    ngAfterViewInit(): void {
        // @ts-ignore
        $('.tabs').tabs();
    }

}
