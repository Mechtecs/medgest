import {Component} from '@angular/core';
import {Tab, HasTabs} from "./tab";
import {ActivatedRoute, NavigationEnd, Router, RouterEvent} from "@angular/router";
import {TabsService} from "./tabs.service";

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styles: []
})
export class AppComponent {

    constructor(public tabs: TabsService) {

    }
}
