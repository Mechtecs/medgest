import {AfterViewInit, Component, OnInit} from '@angular/core';
import {TabsService} from "../tabs.service";

@Component({
    selector: 'app-medienkompetenz-page',
    templateUrl: './medienkompetenz-page.component.html',
    styleUrls: ['./medienkompetenz-page.component.scss']
})
export class MedienkompetenzPageComponent implements AfterViewInit {

    constructor(private tabs: TabsService) {
        tabs.setTabs([
            {
                id: 'tab1',
                title: 'Infoplakat Fake-News',
            },
            {
                id: 'tab2',
                title: 'Kurzreferat Fake-News',
            },
        ]);
    }


    ngAfterViewInit(): void {
        // @ts-ignore
        $('.tabs').tabs();
    }

}
