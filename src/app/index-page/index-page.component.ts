import { Component, OnInit } from '@angular/core';
import {HasTabs, Tab} from "../tab";

@Component({
  selector: 'app-index-page',
  templateUrl: './index-page.component.html',
  styleUrls: ['./index-page.component.scss']
})
export class IndexPageComponent implements HasTabs {

  constructor() { }

  getTabs(): Tab[] {
    return [
      {
        title: 'Pups',
        id: 'pups',
      }
    ];
  };

}
