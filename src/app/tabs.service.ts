import { Injectable } from '@angular/core';
import {NavigationStart, Router} from "@angular/router";
import {Tab} from "./tab";

@Injectable({
  providedIn: 'root'
})
export class TabsService {

  public tabs: Tab[] = [];

  constructor(private router: Router) {
    router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        this.tabs = [];
      }
    });
  }

  public setTabs(param: Tab[]): void {
    this.tabs = param;
  }
}
