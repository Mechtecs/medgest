import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-pdf',
  templateUrl: './pdf.component.html',
  styleUrls: ['./pdf.component.scss']
})
export class PDFComponent implements OnInit {

  @Input('file') public file: string = '';

  constructor() { }

  ngOnInit(): void {
  }

}
