export interface Tab {
    id: string;
    title: string;
}

export interface HasTabs {
    getTabs(): Tab[];
}